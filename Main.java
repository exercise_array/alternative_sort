import java.util.Arrays;

public class Main {

    public static void alternativeSorted(int[] arr, int n) {
        Arrays.sort(arr);
        for(int i = 0; i < n/2; i++) {
            System.out.print(arr[n - 1 -i] + " ");
            System.out.print(arr[i] + " ");
        }
        if (n%2 != 0){
            System.out.println(arr[n/2]);
        }
    }
    public static void main(String[] args) {
        int[] arr = {6, 2, 8, 4, 9, 3, 0, 4};
        alternativeSorted(arr, arr.length);
    }
}
